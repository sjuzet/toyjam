using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leg : MonoBehaviour {
    public GameObject hand;
    public GameObject spike;
    public GameObject trailRenderer;
    public Peg peg;
}