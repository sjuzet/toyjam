using UnityEngine;

public class TwirlyCharacterControllerSettings : ScriptableObject {    
    
    public LayerMask nodeLayerMask;
    public float triggerCheckRadius = 0.25f;
    
    [Space]
    public float fixedGait = 1;
    public float fixedAngularVelocity = 1000;
}