using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using InControl;

public class TwirlyCharacterController : MonoBehaviour {
    public TwirlyCharacterController other;
    public TwirlyCharacterControllerSettings settings;
    
    [Space]
    public KeyCode inputSpinLeft;
    public KeyCode inputSpinRight;
    public KeyCode inputSpike;
    public KeyCode inputGrab;

    [Space]
    public GameObject body;
    public Leg leftLeg;
    public Leg rightLeg;
    public Leg spikeLeg => spikeIsLeftLeg ? leftLeg : rightLeg;
    public Leg swingingLeg => !spikeIsLeftLeg ? leftLeg : rightLeg;
    
    [Space]
    public bool autoGrabbing;
    public bool spinningLeft;
    public bool spiked;
    public bool spikeIsLeftLeg;
    public float angle;
    public float angularVelocity;
    public Peg currentPeg;
    public bool grabbing => currentPeg != null;
    
    [Space]
    public LineRenderer lineRenderer;
    
    void OnEnable () {
        OnSetPivotLeg();
    }

    void Update() {
        if(!grabbing) ManualUpdate();
    }
    void LateUpdate() {
        if(grabbing) ManualUpdate();
    }

    void ManualUpdate() {
        var peg = IsLegInTrigger(swingingLeg.transform);
        if(Input.GetKeyDown(inputSpinLeft)) {
            spinningLeft = true;
        }
        if(Input.GetKeyDown(inputSpinRight)) {
            spinningLeft = false;
        }
        if(spiked) {
            angularVelocity = settings.fixedAngularVelocity * (spinningLeft ? -1 : 1);
        } else if(grabbing) {
            angularVelocity = settings.fixedAngularVelocity * (spinningLeft ? -1 : 1);
        } else {
            angularVelocity = 0;
        }
        if(Input.GetKeyDown(inputGrab)) {
            if(grabbing) {
                Ungrab();
            } else {
                autoGrabbing = !autoGrabbing;
            }
        }
        if(autoGrabbing) {
            if(peg != null && !other.grabbing) {
                Grab(peg);
            }
        }

        if(Input.GetKeyDown(inputSpike)) {
            if(spiked) {
                spiked = false;
            } else {
                Ungrab();
                spiked = true;
            }
        }

        swingingLeg.spike.SetActive(false);
        swingingLeg.hand.SetActive(autoGrabbing);
        
        spikeLeg.spike.SetActive(spiked);
        spikeLeg.hand.SetActive(false);
        
        if(currentPeg != null) {
            spikeLeg.transform.position = currentPeg.transform.position;
        }
        
        angle += angularVelocity * Time.deltaTime;
        angle = MathX.WrapDegrees(angle);

        swingingLeg.transform.position = spikeLeg.transform.position + Vector3X.CreateXZWithDegrees(angle) * settings.fixedGait;

        lineRenderer.SetPosition(0, leftLeg.transform.position);
        lineRenderer.SetPosition(1, rightLeg.transform.position);

        body.transform.position = Vector3.Lerp(leftLeg.transform.position, rightLeg.transform.position, 0.5f);
        body.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }

    void Ungrab () {
        currentPeg = null;
    }
    void Grab (Peg peg) {
        spiked = false;
        autoGrabbing = false;
        // spikeIsLeftLeg = false;
        // OnSetPivotLeg();
        currentPeg = peg;
    }

    void OnSetPivotLeg () {
        spikeLeg.trailRenderer.SetActive(true);
        swingingLeg.trailRenderer.SetActive(false);

        spikeLeg.peg.gameObject.SetActive(false);
        swingingLeg.peg.gameObject.SetActive(true);
    }

    Peg IsLegInTrigger (Transform leg) {
        var overlap = Physics.OverlapSphere(leg.position, settings.triggerCheckRadius, settings.nodeLayerMask, QueryTriggerInteraction.Collide);
        if(overlap.Length > 0) {
            var overlapy = overlap.Where(x => !x.transform.IsDescendentOf(transform)).FirstOrDefault();
            if(overlapy != null) {
                return overlapy.GetComponent<Peg>();
            }
        }
        return null;
    }
}
